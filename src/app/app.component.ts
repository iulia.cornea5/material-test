import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'material-test';

  cats: Array<string> = ["Puffy", "Sloppy", "Oppenheimer", "Sassy","Puffy", "Sloppy", "Oppenheimer", "Sassy", "Mikey"]
}
