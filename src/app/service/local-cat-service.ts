import { Injectable } from "@angular/core";
import { Cat, NewCat } from "../model/cat";
import { LocalData } from "../model/local-data"

@Injectable(
    {
        providedIn: 'root'
    }
)
export class LocalCatService {

    constructor() {
    }

    createCatLocal(newCat: NewCat): Cat {
        var cat = newCat as Cat
        cat.id = LocalData.localCats.length
        LocalData.localCats.push(cat)
        return cat
    }

    getCatLocal(id: number): Cat {
        console.log(LocalData.localCats.find(c => {
            console.log(c.id)
            console.log(id)
            console.log(c.id == id)
            return c.id == id
        }))
        return LocalData.localCats.find(c => { return c.id == id })
    }

    getCatsLocal(): Array<Cat> {
        return LocalData.localCats
    }

    updateCatLocal(cat: Cat): Cat {
        console.log(cat)
        const index = LocalData.localCats.findIndex(c => { return c.id == cat.id })
        LocalData.localCats[index] = cat
        console.log(LocalData.localCats)
        return LocalData.localCats[index]
    }

    // deleteCatLocal(id: number): Cat {
    //     return null
    // }

    deleteCatLocal(id: number): boolean {
        const index = LocalData.localCats.findIndex(c => { return c.id == id })
        LocalData.localCats.splice(index, 1)
        return true
    }



}