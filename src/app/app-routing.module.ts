import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatFormComponent } from './cat-form/cat-form.component';
import { CatListComponent } from './cat-list/cat-list.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: "home", component: HomeComponent},
  {path: "cats", component: CatListComponent},
  {path: "new-cat", component: CatFormComponent},
  {path: "", redirectTo: 'home', pathMatch: "full"},
  {path: "cats/:id", component: CatFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
