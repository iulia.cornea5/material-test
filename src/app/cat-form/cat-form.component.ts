import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Cat, NewCat } from '../model/cat';
import { LocalCatService } from '../service/local-cat-service';

@Component({
  selector: 'app-cat-form',
  templateUrl: './cat-form.component.html',
  styleUrls: ['./cat-form.component.css']
})
export class CatFormComponent implements OnInit {

  newCat: NewCat = { name: "", breed: "", likes: 0 }
  cat: Cat

  nameFormControl = new FormControl()
  breedFormControl = new FormControl()
  descriptionFormControl = new FormControl()


  constructor(
    private localCatService: LocalCatService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    
    var routeSnapshot = this.activatedRoute.snapshot
    const catId: number = routeSnapshot.params['id']
    if(!!catId) {
      this.cat = this.localCatService.getCatLocal(catId)

      console.log(this.cat)
      this.nameFormControl.setValue(this.cat.name)
      this.breedFormControl.setValue(this.cat.breed)
      this.descriptionFormControl.setValue(this.cat.description)
    }
  }

  saveCat() {
    if(!!this.cat) {
      this.cat.name = this.nameFormControl.value
      this.cat.breed = this.breedFormControl.value
      this.cat.description = this.descriptionFormControl.value
      this.localCatService.updateCatLocal(this.cat)
    } else {
      this.newCat.name = this.nameFormControl.value
      this.newCat.breed = this.breedFormControl.value
      this.newCat.description = this.descriptionFormControl.value
      this.localCatService.createCatLocal(this.newCat)
    }
    this.router.navigate(["cats"])
  }

}
