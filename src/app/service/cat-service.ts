import { Cat, NewCat } from "../model/cat";

export class CatService {
    constructor () {

    }
    
    createCat(cat: NewCat): Cat {
        return null
    }

    getCat(id: number): Cat {
        return null
    }

    getCats(): Array<Cat> {
        return []
    }

    updateCat(cat: Cat): Cat {
        return null
    }

    // deleteCat(id: number): Cat {
    //     return null
    // }

    deleteCat(id: number): boolean {
        return false
    }
}