import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cat } from '../model/cat';

@Component({
  selector: 'app-cat-card',
  templateUrl: './cat-card.component.html',
  styleUrls: ['./cat-card.component.css']
})
export class CatCardComponent implements OnInit {

  @Input("cat")
  cat: Cat

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectToEdit(){
    this.router.navigate(["/cats/" + this.cat.id]) 
  }
  
  upvote() {
    this.cat.likes++
  }
  
  downvote() {
    this.cat.likes--
  }
}
