import { Component, OnInit } from '@angular/core';
import { Cat } from 'src/app/model/cat'
import { LocalCatService } from 'src/app/service/local-cat-service'

@Component({
  selector: 'app-cat-list',
  templateUrl: './cat-list.component.html',
  styleUrls: ['./cat-list.component.css']
})
export class CatListComponent implements OnInit {

  cats: Array<Cat> = []

  constructor(private localCatService: LocalCatService ) { }

  ngOnInit(): void {
    this.populateCats()
  }

  populateCats() {
    this.cats = this.localCatService.getCatsLocal()
  }


}
